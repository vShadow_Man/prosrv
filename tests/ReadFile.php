<?php
declare(strict_types=1);

namespace App\Tests;

trait ReadFile
{
    private static function readFile(string $filename): string
    {
        return file_get_contents(
            sprintf('/application/tests/resources/JsonPayloads/%s', $filename)
        );
    }
}