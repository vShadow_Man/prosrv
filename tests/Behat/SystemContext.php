<?php
declare(strict_types=1);

namespace App\Tests\Behat;

use App\Kernel;
use Behat\Behat\Context\Context;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;

final class SystemContext implements Context
{
    private KernelInterface $kernel;
    private Application $application;
    private BufferedOutput $output;

    //public function __construct(KernelInterface $kernel)
    public function __construct()
    {
        $this->kernel = new Kernel('test', true);;
        $this->kernel->boot();
        $this->application = new Application($this->kernel);
//        $this->kernel = $kernel;
//        $this->application = new Application($kernel);
        $this->output = new BufferedOutput();
    }

}