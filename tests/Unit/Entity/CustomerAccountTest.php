<?php
declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Entity\CustomerAccount;
use App\Tests\TestDataGenerator;
use PHPUnit\Framework\TestCase;

final class CustomerAccountTest extends TestCase
{
    public const DATA = [
        'account_no' => 'my_acc_no',
        'customer_name' => 'alpha bravo',
        'sort_code' => 123456,
        'currency' => 'GBP',
    ];
    private const NEW_TRANSACTIONS_COUNT = 5;
    
    public function testConstructor(): void
    {
        $this->assertGetters(self::DATA, TestDataGenerator::createCustomerAccount(self::DATA));
    }

    public function testSetter(): void
    {
        $customerAccount = new CustomerAccount('', '', 0, '');
        $customerAccount->setAccountNo(self::DATA['account_no']);
        $customerAccount->setCustomerName(self::DATA['customer_name']);
        $customerAccount->setSortCode(self::DATA['sort_code']);
        $customerAccount->setCurrency(self::DATA['currency']);

        $this->assertGetters(self::DATA, $customerAccount);
    }

    public function testTransactions(): void
    {
        $customerAccount = TestDataGenerator::createCustomerAccount(self::DATA);
        //assert no transactions
        $this->assertEquals(0, $customerAccount->getTransactions()->count());

        //add new transactions
        for($int = 0; $int < self::NEW_TRANSACTIONS_COUNT; $int++) {
            $customerAccount->addTransaction(
                TestDataGenerator::createTransaction(
                    TestDataGenerator::generateTransactionTestData($customerAccount)
                )
            );
        }

        //assert transactions count
        $this->assertEquals(self::NEW_TRANSACTIONS_COUNT, $customerAccount->getTransactions()->count());

        //assert transaction removed
        $firstTransaction = $customerAccount->getTransactions()[0];
        $customerAccount->removeTransaction($firstTransaction);
        $this->assertEquals(self::NEW_TRANSACTIONS_COUNT - 1, $customerAccount->getTransactions()->count());
        $this->assertNotEquals($firstTransaction, $customerAccount->getTransactions()[0]);
    }

    private function assertGetters(array $expected, CustomerAccount $customerAccount): void
    {
        $this->assertEquals($expected['account_no'], $customerAccount->getAccountNo());
        $this->assertEquals($expected['customer_name'], $customerAccount->getCustomerName());
        $this->assertEquals($expected['sort_code'], $customerAccount->getSortCode());
        $this->assertEquals($expected['currency'], $customerAccount->getCurrency());
    }
}