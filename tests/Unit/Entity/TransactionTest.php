<?php
declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Entity\CustomerAccount;
use App\Entity\Transaction;
use App\Services\DateHelper;
use App\Tests\TestDataGenerator;
use PHPUnit\Framework\TestCase;

final class TransactionTest extends TestCase
{
    public function testConstructor(): void
    {
        $data = $this->getTestData();
        $this->assertGetters($data, TestDataGenerator::createTransaction($data));
    }

    private function getTestData(): array
    {
        return [
            'id' => 999,
            'customer_account' => TestDataGenerator::createCustomerAccount(CustomerAccountTest::DATA),
            'timestamp' => new \DateTimeImmutable(),
            'type' => TestDataGenerator::getRandomTransactionType(),
            'amount' => 123.45,
            'description' => 'foo bar',
            'iban' => 'GB53BARC20032622490001',
        ];
    }

    public function testSetter(): void
    {
        $customerAccount = new CustomerAccount('foo_acc_no', 'foo_customer', 102030, 'EUR');
        $transaction = new Transaction(
            null,
            $customerAccount,
            new \DateTimeImmutable('2014-06-20 11:45 Europe/London'),
            '',
            0,
            null,
            ''
        );

        $data = $this->getTestData();
        $transaction->setId($data['id']);
        $transaction->setCustomerAccount($data['customer_account']);
        $transaction->setTimestamp($data['timestamp']);
        $transaction->setType($data['type']);
        $transaction->setAmount($data['amount']);
        $transaction->setDescription($data['description']);
        $transaction->setIban($data['iban']);

        $this->assertGetters($data, $transaction);
    }

    private function assertGetters(array $expected, Transaction $transaction): void
    {
        $this->assertEquals($expected['id'], $transaction->getId());
        $this->assertEquals($expected['customer_account'], $transaction->getCustomerAccount());
        $this->assertEquals(
            $expected['timestamp']->format(DateHelper::SHORT_DATE_TIME_FORMAT),
            $transaction->getTimestamp()
        );
        $this->assertEquals($expected['type'], $transaction->getType());
        $this->assertEquals($expected['amount'], $transaction->getAmount());
        $this->assertEquals($expected['description'], $transaction->getDescription());
        $this->assertEquals($expected['iban'], $transaction->getIban());
    }
}