<?php
declare(strict_types=1);

namespace App\Tests\Unit\Services;

use App\Entity\CustomerAccount;
use App\Services\Serialiser;
use App\Tests\ReadFile;
use App\Tests\TestDataGenerator;
use App\Tests\Unit\Entity\CustomerAccountTest;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

final class SerialiserTest extends TestCase
{
    use ReadFile;

    public function testSerialiseEntityFromJson(): void
    {
        $serialiser = self::createSerialiser();
        $json = self::readFile('new_customer_account.json');
        $expected = json_decode($json, true);

        /** @var CustomerAccount $result */
        $result = $serialiser->serialiseEntityFromJson($json, CustomerAccount::class);
        $this->assertEquals($expected['customerName'], $result->getCustomerName());
        $this->assertEquals($expected['accountNo'], $result->getAccountNo());
        $this->assertEquals($expected['sortCode'], $result->getSortCode());
        $this->assertEquals($expected['currency'], $result->getCurrency());
    }

    public function testSerialiseEntityFromJsonMalformed(): void
    {
        $serialiser = self::createSerialiser();
        $invalidJson = '{"key": "value"}}';

        $this->expectException(NotEncodableValueException::class);
        $serialiser->serialiseEntityFromJson($invalidJson, CustomerAccount::class);
    }

    public function testSerialiseArrayFromEntity(): void
    {
        $serialiser = self::createSerialiser();
        $entity = TestDataGenerator::createCustomerAccount(CustomerAccountTest::DATA);

        $result = $serialiser->serialiseArrayFromEntity($entity);
        $this->assertCount(5, $result);
        $this->assertEquals($entity->getCustomerName(), $result['customerName']);
        $this->assertEquals($entity->getAccountNo(), $result['accountNo']);
        $this->assertEquals($entity->getSortCode(), $result['sortCode']);
        $this->assertEquals($entity->getCurrency(), $result['currency']);
        $this->assertEquals($entity->getTransactions()->toArray(), $result['transactions']);

        //@todo: check with different group names
    }

    private static function createSerialiser(): Serialiser
    {
        $serializer = new SymfonySerializer([new ObjectNormalizer()], [new JsonEncoder()]);
        return new Serialiser($serializer);
    }
}