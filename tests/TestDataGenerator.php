<?php
declare(strict_types=1);

namespace App\Tests;

use App\Entity\CustomerAccount;
use App\Entity\Transaction;
use App\Tests\Unit\Entity\CustomerAccountTest;
use Faker\Factory;

final class TestDataGenerator
{
    public static function createCustomerAccount(array $data): CustomerAccount
    {
        return new CustomerAccount(
            $data['account_no'],
            $data['customer_name'],
            $data['sort_code'],
            $data['currency'],
        );
    }

    public static function createTransaction(array $data): Transaction
    {
        return new Transaction(
            $data['id'],
            $data['customer_account'],
            $data['timestamp'],
            $data['type'],
            $data['amount'],
            $data['description'],
            $data['iban'],
        );
    }

    public static function generateTransactionTestData(?CustomerAccount $customerAccount = null): array
    {
        $faker = Factory::create();

        return [
            'id' => $faker->numberBetween(100, 999),
            'customer_account' => $customerAccount ?? self::createCustomerAccount(CustomerAccountTest::DATA),
            'timestamp' => new \DateTimeImmutable($faker->date()),
            'type' => self::getRandomTransactionType(),
            'amount' => $faker->numberBetween(1,999),
            'description' => $faker->realText(255),
            'iban' => $faker->regexify('[A-Za-z0-9]{32}'),
        ];
    }

    public static function getRandomTransactionType(): string
    {
        return ((bool)random_int(0, 1)) ? Transaction::CREDIT_TYPE : Transaction::DEBIT_TYPE;
    }
}