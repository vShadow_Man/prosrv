<?php
declare(strict_types=1);

namespace App\Tests\Integration;

use App\DataFixtures\AppFixtures;
use App\Services\DateHelper;
use App\Tests\ReadFile;
use Symfony\Component\HttpFoundation\Response;

final class TransactionControllerTest extends EndpointTest
{
    use ReadFile;

    private const EXPECTED_TRANSACTION_PROPERTIES = [
        'id',
        'timestamp',
        'type',
        'amount',
        'description',
        'iban',
    ];
    private const SHORT_TIMESTAMP = 'Y-m-d H:i';
    private array $expectedTransactions;
    private ?int $customerAccountNo;

    public function setUp()
    {
        parent::setUp();

        $this->customerAccountNo = key(AppFixtures::TEST_ACCOUNTS);
        //get reference copy of transactions belong to first test account
        $this->expectedTransactions = $this->executeNativeQuery(
            'SELECT * FROM transaction t WHERE t.account_no = ?',
            [$this->customerAccountNo]
        );
    }

    public function testShowEndpoint(): void
    {
        $this->assertTransactionIsRetrieved($this->customerAccountNo, $this->expectedTransactions[0]);
    }

    public function testShowAllByCustomerAccount(): void
    {
        $customerAccountNo = array_keys(AppFixtures::TEST_ACCOUNTS)[5];
        $endpoint = sprintf('%s/transaction', self::buildBaseEndpointUsingAccount($customerAccountNo));
        $this->client->request('GET', $endpoint);
        $result = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(AppFixtures::TEST_ACCOUNTS[$customerAccountNo], $result);
        $this->assertEachItemInCollectionHasGivenProperties($result, self::EXPECTED_TRANSACTION_PROPERTIES);
    }

    public function testCreateEndpoint(): void
    {
        $customerAccountNo = 12345689;
        $json = self::readFile('new_transaction.json');

        $this->client->request(
            'POST', self::buildBaseEndpointForTransaction($customerAccountNo),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $json
        );
        $result = json_decode($this->client->getResponse()->getContent(), true);

        $expected = json_decode($json, true);
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertTransactionProperties($result, $expected);
    }

    public function testUpdateEndpoint(): void
    {
        $customerAccountNo = array_keys(AppFixtures::TEST_ACCOUNTS)[5];
        $json = self::readFile('update_transaction.json');

        $this->client->request(
            'PUT',
            self::buildBaseEndpointForTransaction($customerAccountNo, (int) $this->expectedTransactions[0]['id']),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $json
        );
        $result = json_decode($this->client->getResponse()->getContent(), true);
        $expected = json_decode($json, true);
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertTransactionProperties($result, $expected);
    }

    public function testDeleteEndpoint(): void
    {
        $customerAccountNo = array_keys(AppFixtures::TEST_ACCOUNTS)[2];
        $endpoint = self::buildBaseEndpointForTransaction(
            $customerAccountNo,
            (int) $this->expectedTransactions[0]['id']
        );

        $this->client->request('DELETE', $endpoint);
        $result = json_decode($this->client->getResponse()->getContent(), true);
        $expected = sprintf('Transaction %s has been deleted', $this->expectedTransactions[0]['id']);

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($expected, $result['message']);

        //assert that it cannot be deleted again
        $this->client->request('DELETE', $endpoint);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());

        //assert that it cannot be retrieved
        $this->client->request('GET', $endpoint);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    private function assertTransactionIsRetrieved(
        int $customerAccountNo,
        array $expectedTransaction
    ): void {
        $this->client->request('GET', self::buildBaseEndpointForTransaction(
            $customerAccountNo,
            (int) $expectedTransaction['id'])
        );
        $result = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTransactionProperties($result, $expectedTransaction);
    }

    private function assertTransactionProperties(array $result, array $expectedTransaction): void
    {
        if (isset($expectedTransaction['id'])) {
            $this->assertEquals($expectedTransaction['id'], $result['id']);
        }
        $expectedTimeStamp = \DateTimeImmutable::createFromFormat(
            DateHelper::LONG_DATE_TIME_FORMAT,
            $expectedTransaction['timestamp']
        );
        $resultTimeStamp = \DateTimeImmutable::createFromFormat(
            DateHelper::SHORT_DATE_TIME_FORMAT,
            $result['timestamp']
        );

        $this->assertEquals(
            $expectedTimeStamp->format(self::SHORT_TIMESTAMP),
            $resultTimeStamp->format(self::SHORT_TIMESTAMP)
        );
        $this->assertEquals($expectedTransaction['type'], $result['type']);
        $this->assertEquals($expectedTransaction['amount'], number_format((float) $result['amount'], 2, '.', ''));
        $this->assertEquals($expectedTransaction['description'], $result['description']);
        $this->assertEquals($expectedTransaction['iban'], $result['iban']);
        $this->assertCount(6, $result);
    }

    private static function buildBaseEndpointForTransaction(int $customerAccountNo, ?int $transactionId = null): string
    {
        $transactionSegment = ($transactionId === null) ? '' : sprintf('/%s', $transactionId);
        return sprintf(
            '%s/transaction%s',
            self::buildBaseEndpointUsingAccount($customerAccountNo),
            $transactionSegment
        );
    }
}