<?php
declare(strict_types=1);

namespace App\Tests\Integration;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class EndpointTest extends WebTestCase
{
    private const REQUEST_SECURITY_TOKEN = 'my_test_security_token';
    protected KernelBrowser $client;

    public function setUp()
    {
        $this->client = static::createClient([], ['HTTP_TOKEN' => self::REQUEST_SECURITY_TOKEN]);
    }

    protected static function buildBaseEndpointUsingAccount(int $customerAccountNo = null): string {
        return sprintf('/v1/account/%d', $customerAccountNo);
    }

    protected function executeNativeQuery(string $sql, array $params): array
    {
        $doctrine =  $this->client->getContainer()->get('doctrine');

        return $doctrine->getConnection()->fetchAll($sql, $params);
    }

    protected function assertEachItemInCollectionHasGivenProperties(array $collection, array $expectedProperties): void
    {
        array_walk($collection, function(array $item) use ($expectedProperties) {
            $this->assertEmpty(array_diff_key(array_flip($expectedProperties), $item));
        });
    }
}