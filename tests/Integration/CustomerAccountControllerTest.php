<?php
declare(strict_types=1);

namespace App\Tests\Integration;

use App\DataFixtures\AppFixtures;
use App\Tests\ReadFile;
use Symfony\Component\HttpFoundation\Response;

final class CustomerAccountControllerTest extends EndpointTest
{
    use ReadFile;

    private const EXPECTED_CUSTOMER_ACCOUNT_PROPERTIES = [
        'accountNo',
        'customerName',
        'sortCode',
        'currency',
        'transactions',
    ];

    public function testShowEndpoint(): void
    {
        $customerAccountNo = key(AppFixtures::TEST_ACCOUNTS);
        $expectedTransactionsCount = AppFixtures::TEST_ACCOUNTS[$customerAccountNo];

        $this->assertCustomerAccountIsRetrieved($customerAccountNo, $expectedTransactionsCount);
    }

    public function testShowAllEndpoint(): void
    {
        $this->client->request('GET', '/v1/account');
        $result = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(count(AppFixtures::TEST_ACCOUNTS), $result);
        $this->assertEachItemInCollectionHasGivenProperties($result, self::EXPECTED_CUSTOMER_ACCOUNT_PROPERTIES);
    }

    public function testCreateEndpoint(): void
    {
        $customerAccountNo = 12345689;
        $json = self::buildJsonPayloadForCustomerAccount($customerAccountNo);

        $this->client->request(
            'POST', '/v1/account',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $json
        );
        $result = json_decode($this->client->getResponse()->getContent(), true);

        $expected = json_decode($json, true);
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertCustomerAccountProperties($result, $expected, 0);
    }

    public function testUpdateEndpoint(): void
    {
        $customerAccountNo = array_keys(AppFixtures::TEST_ACCOUNTS)[1];
        $expectedTransactionsCount = AppFixtures::TEST_ACCOUNTS[$customerAccountNo];
        $json = self::buildJsonPayloadForCustomerAccount($customerAccountNo);

        $this->client->request(
            'PUT', self::buildBaseEndpointUsingAccount($customerAccountNo),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $json
        );
        $result = json_decode($this->client->getResponse()->getContent(), true);

        $expected = json_decode($json, true);
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertCustomerAccountProperties($result, $expected, $expectedTransactionsCount);
    }

    public function testDeleteEndpoint(): void
    {
        $customerAccountNo = array_keys(AppFixtures::TEST_ACCOUNTS)[2];

        $this->client->request('DELETE', self::buildBaseEndpointUsingAccount($customerAccountNo));
        $result = json_decode($this->client->getResponse()->getContent(), true);
        $expected = sprintf('Customer account %s has been deleted', $customerAccountNo);

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($expected, $result['message']);

        //assert that it cannot be deleted again
        $this->client->request('DELETE', self::buildBaseEndpointUsingAccount($customerAccountNo));
        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());

        //assert that it cannot be retrieved
        $this->client->request('GET', self::buildBaseEndpointUsingAccount($customerAccountNo));
        $this->assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    private function assertCustomerAccountProperties(
        array $result,
        array $expected,
        int $expectedTransactionsCount
    ): void
    {
        $this->assertCount(5, $result);
        $this->assertEquals($expected['customerName'], $result['customerName']);
        $this->assertEquals($expected['sortCode'], $result['sortCode']);
        $this->assertEquals($expected['currency'], $result['currency']);
        $this->assertCustomerAccountIsRetrieved((int) $expected['accountNo'], $expectedTransactionsCount);
    }

    private function assertCustomerAccountIsRetrieved(
        int $customerAccountNo,
        ?int $expectedTransactionsCount = null
    ): void {
        $this->client->request('GET', self::buildBaseEndpointUsingAccount($customerAccountNo));
        $result = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(5, $result);
        if ($expectedTransactionsCount !== null) {
            $this->assertCount($expectedTransactionsCount, $result['transactions']);
        }
    }

    private static function buildJsonPayloadForCustomerAccount(int $customerAccountNo): string
    {
        return sprintf(
            self::readFile('update_customer_account.json'),
            $customerAccountNo
        );
    }
}