<?php
declare(strict_types=1);

namespace App\Tests\Integration;

final class HealthControllerTest extends EndpointTest
{
    public function testHealthEndpoint(): void
    {
        $this->client->request('GET', '/health');
        $result = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(2, $result);
        $this->assertEquals('Service is running', $result['app']);
        $this->assertEquals('Successfully connected to DB', $result['db']);
    }
}