Feature: Retrieve all customer accounts
  In order to see how many customer accounts there are
  As an API client
  I want retrieve all customer accounts

  Scenario: Retrieve all customer accounts
    Given I add "token" header equal to "my_test_security_token"
      When I send a GET request to "/v1/account"
      Then the response status code should be 200
      And the response should be in JSON
      And the JSON node '' should have 10 elements
      And the JSON should be valid according to this schema:
              """
                {
                  "$schema": "http://json-schema.org/draft-04/schema#",
                  "type": "array",
                  "items": [
                    {
                      "type": "object",
                      "required":true,
                      "properties": {
                        "accountNo": {
                          "type": "string",
                          "required":true
                        },
                        "customerName": {
                          "type": "string",
                          "required":true
                        },
                        "sortCode": {
                          "type": "integer",
                          "required":true
                        },
                        "currency": {
                          "type": "string",
                          "required":true
                        },
                        "transactions": {
                          "type": "array",
                          "items": [
                            {
                              "type": "object",
                              "properties": {
                                "id": {
                                  "type": "integer",
                                  "required":true
                                },
                                "timestamp": {
                                  "type": "string",
                                  "required":true
                                },
                                "type": {
                                  "type": "string",
                                  "required":true
                                },
                                "amount": {
                                  "type": "integer",
                                  "required":true
                                },
                                "description": {
                                  "type": "string",
                                  "required":true
                                },
                                "iban": {
                                  "type": "string",
                                  "required":true
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  ]
                }
              """

