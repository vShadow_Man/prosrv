Feature: Retrieve a specific customer account
  In order to see details for a specific customer account
  As an API client
  I want retrieve a customer account using an account no

  Scenario: Retrieve specific customer account
    Given I add "token" header equal to "my_test_security_token"
      When I send a GET request to "/v1/account/12345678"
      Then the response status code should be 200
      And the response should be in JSON
      And the JSON node '' should have 5 elements
	  And the JSON node "accountNo" should contain "12345678"
	  And the JSON should be valid according to this schema:
              """
                {
                  "$schema": "http://json-schema.org/draft-04/schema#",
				  "type": "object",
				  "required":true,
				  "properties": {
						"accountNo": {
						  "type": "string",
						  "required":true
						},
						"customerName": {
						  "type": "string",
						  "required":true
						},
						"sortCode": {
						  "type": "integer",
						  "required":true
						},
						"currency": {
						  "type": "string",
						  "required":true
						},
						"transactions": {
						  "type": "array"
						}
					}
                }
              """

  Scenario: Fail to retrieve a specific customer account that does not exist
    Given I add "token" header equal to "my_test_security_token"
      When I send a GET request to "/v1/account/1111111"
      Then the response status code should be 404
      And the response should be in JSON