Feature: Confirm that API service is running
  In order to confirm that the API service is running
  As an API client
  I want to check the health endpoint, to see that all sub-services are running

  Scenario: Check health of API service
    Given I add "token" header equal to "my_test_security_token"
    When I send a GET request to "/health"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node '' should have 2 elements
    And the JSON node "app" should contain "Service is running"
    And the JSON node "db" should contain "Successfully connected to DB"