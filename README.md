# README #

This is a simple CRUD REST API to aid me in personal development, using best practices in Symfony.
It will also be used as a test repo for CI/CD deployment.

### Usage ###
1. Clone the repo onto your local machine and `cd` to the installed directory.

2. [Install Docker](https://docs.docker.com/install) and [Docker Compose](https://docs.docker.com/compose/install/). Run `make start` 
to download, build and spin-up all the docker containers. 

3. Install the composer dependencies by `make composer-install`. 

4. Run migrations by `make migrate`.

4. Import into [Postman](https://www.postman.com/downloads/) the collection at `tests/resources/postman_collection.json`.

5. Check the REST API service is working by trying `/health` endpoint from the Postman collection. 

6. If you want to populate the database with test data, use `make load-fixtures`. You can then try the 
other endpoints, via the Postman collection. This will overwrite any existing data.

### Usage ###
Use the Postman collection to make requests.

To view available terminal commands, use `make`.
### Tests ###

To run units tests, use `make run-unit-tests`.

To run behat tests, use `make run-behat-tests`.

To all tests, use `make run-all-tests`.
