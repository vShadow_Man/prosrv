<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CustomerAccount;
use App\Entity\EntityCreationException;
use App\Entity\EntityDeleteException;
use App\Entity\EntityFetchException;
use App\Entity\EntityUpdateException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomerAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerAccount[]    findAll()
 * @method CustomerAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class CustomerAccountRepository extends ServiceEntityRepository implements RepositoryFindOne
{
    private const PRIMARY_KEY = 'accountNo';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerAccount::class);
    }

    /**
     * @throws EntityFetchException
     */
    public function findOne(string $id): CustomerAccount
    {
        $customerAccount = $this->findOneBy([self::PRIMARY_KEY => $id]);
        if (!$customerAccount) {
            throw new EntityFetchException(
                sprintf(
                    'Could not retrieve customer account. %s',
                    $id
                )
            );
        }

        return $customerAccount;
    }

    /**
     * @throws EntityCreationException
     */
    public function create(CustomerAccount $entity): CustomerAccount
    {
        try {
            $this->_em->persist($entity);
            $this->_em->flush();
        } catch (ORMException $exception) {
            throw new EntityCreationException(
                sprintf(
                    'There was a problem trying to create this customer account. %s',
                    $exception->getMessage()
                )
            );
        }

        return $entity;
    }

    /**
     * @throws EntityUpdateException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(CustomerAccount $entity): CustomerAccount
    {
        $customerAccount = $this->_em
            ->getRepository(CustomerAccount::class)
            ->find($entity->getAccountNo());

        if (!$customerAccount) {
            throw new EntityUpdateException(
                sprintf(
                    'Customer account not found for %s',
                    $entity->getAccountNo()
                )
            );
        }

        $customerAccount->setCustomerName($entity->getCustomerName());
        $customerAccount->setSortCode($entity->getSortCode());
        $customerAccount->setCurrency($entity->getCurrency());
        $this->_em->flush();

        return $customerAccount;
    }

    /**
     * @throws EntityDeleteException
     * @throws EntityFetchException
     */
    public function delete(string $id): bool
    {
        $customerAccount = $this->findOne($id);
        try {
            $this->_em->remove($customerAccount);
            $this->_em->flush();
        } catch (OptimisticLockException | ORMException $exception) {
            throw new EntityDeleteException(
                sprintf(
                    'Customer account %s could not be deleted. %s',
                    $id,
                    $exception->getMessage()
                )
            );
        }

        return true;
    }
}
