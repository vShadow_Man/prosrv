<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Entity;

interface RepositoryFindOne
{
    public function findOne(string $id): Entity;
}
