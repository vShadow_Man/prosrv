<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\EntityCreationException;
use App\Entity\EntityDeleteException;
use App\Entity\EntityFetchException;
use App\Entity\EntityUpdateException;
use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class TransactionRepository extends ServiceEntityRepository implements RepositoryFindOne
{
    private const PRIMARY_KEY = 'id';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    /**
     * @throws EntityFetchException
     */
    public function findOne(string $id): Transaction
    {
        $transaction = $this->findOneBy([self::PRIMARY_KEY => $id]);
        if (!$transaction) {
            throw new EntityFetchException(
                sprintf(
                    'Could not retrieve transaction. %s',
                    $id
                )
            );
        }

        return $transaction;
    }

    /**
     * @throws EntityCreationException
     */
    public function create(Transaction $entity): Transaction
    {
        try {
            $this->_em->persist($entity);
            $this->_em->flush();
        } catch (ORMException $exception) {
            throw new EntityCreationException(
                sprintf(
                    'There was a problem trying to create this transaction. %s',
                    $exception->getMessage()
                )
            );
        }

        return $entity;
    }

    /**
     * @throws EntityUpdateException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(Transaction $entity): Transaction
    {
        $transaction = $this->_em
            ->getRepository(Transaction::class)
            ->find($entity->getId());

        if (!$transaction) {
            throw new EntityUpdateException(
                sprintf(
                    'Transaction not found for %s',
                    $entity->getId()
                )
            );
        }

        $transaction->setCustomerAccount($entity->getCustomerAccount());
        $transaction->setTimestamp($entity->getTimestampObject());
        $transaction->setType($entity->getType());
        $transaction->setAmount($entity->getAmount());
        $transaction->setDescription($entity->getDescription());
        $transaction->setIban($entity->getIban());
        $this->_em->flush();

        return $transaction;
    }

    /**
     * @throws EntityDeleteException
     * @throws EntityFetchException
     */
    public function delete(string $id): bool
    {
        $customerAccount = $this->findOne($id);
        try {
            $this->_em->remove($customerAccount);
            $this->_em->flush();
        } catch (OptimisticLockException | ORMException $exception) {
            throw new EntityDeleteException(
                sprintf(
                    'Transaction %s could not be deleted. %s',
                    $id,
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    public function findAllByCustomerAccount(string $accountId): array
    {
        return $this->findBy(['customerAccount' => $accountId], ['timestamp' => 'DESC']);
    }
}
