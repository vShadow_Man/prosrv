<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Controller\TokenAuthenticatedController;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

final class TokenSubscriber implements EventSubscriberInterface
{
    private array $token;
    private LoggerInterface $logger;

    public function __construct(array $token, LoggerInterface $logger)
    {
        $this->token = $token;
        $this->logger = $logger;
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $this->addLogEntryForRequest($event->getRequest());

        $controller = $event->getController();

        // when a controller class defines multiple action methods, the controller
        // is returned as [$controllerInstance, 'methodName']
        if (is_array($controller)) {
            $controller = $controller[0];
        }

        if ($controller instanceof TokenAuthenticatedController) {
            $token = $event->getRequest()->headers->get('token');
            if (!in_array($token, $this->token)) {
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    private function addLogEntryForRequest(Request $request): void
    {
        $data = [
            'method' => $request->getMethod(),
            'uri' => $request->getUri(),
            'header-token' => $request->headers->get('token'),
        ];
        if (in_array($request->getMethod(), ['POST', 'PUT', 'PATCH'])) {
            $data['body'] = json_decode($request->getContent(), true);
        }
        $this->logger->info('request', $data);
    }
}
