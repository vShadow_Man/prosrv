<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CustomerAccount;
use App\Entity\Transaction;
use App\Tests\TestDataGenerator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Console\Output\ConsoleOutput;

final class AppFixtures extends Fixture
{
    public const TEST_ACCOUNTS = [
      12345678 => 5,
      12345679 => 10,
      12345680 => 3,
      12345681 => 0,
      12345682 => 8,
      12345683 => 3,
      12345684 => 9,
      12345685 => 2,
      12345686 => 4,
      12345687 => 7,
    ];
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $output = new ConsoleOutput();
        $output->writeln(
            sprintf(
                '<info>Preparing to create %d customer accounts</info>',
                count(self::TEST_ACCOUNTS)
            )
        );

        foreach (self::TEST_ACCOUNTS as $accountNumber => $transactionsCount) {
            $output->writeln(sprintf('<info>Creating customer account number %d</info>', $accountNumber));

            $customerAccount = new CustomerAccount(
                (string) $accountNumber,
                $this->faker->name,
                $this->faker->numberBetween(100000, 999999),
                $this->faker->currencyCode,
            );

            $output->writeln(
                sprintf(
                    '<info>Creating %d transactions for this customer account</info>',
                    $transactionsCount
                )
            );
            for ($b = 0; $b < $transactionsCount; $b++) {
                $customerAccount->addTransaction(
                    $this->generateRandomTransaction(
                        TestDataGenerator::generateTransactionTestData($customerAccount)
                    )
                );
            };

            $manager->persist($customerAccount);
        }

        $manager->flush();
    }

    private function generateRandomTransaction(array $data): Transaction
    {
        return new Transaction(
            $data['id'],
            $data['customer_account'],
            $data['timestamp'],
            $data['type'],
            $data['amount'],
            $data['description'],
            $data['iban'],
        );
    }
}
