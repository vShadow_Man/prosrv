<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CustomerAccountRepository;
use App\Services\Serialiser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CustomerAccountRepository::class)
 */
class CustomerAccount implements Entity
{
    /**
     * @ORM\Column(nullable=false, unique=true, length=8)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Groups({"show_detail", "show_list", "new"})
     */
    private string $accountNo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show_detail", "show_list", "new"})
     */
    private string $customerName;

    /**
     * @ORM\Column(type="string", length=6)
     * @Groups({"show_detail", "new"})
     */
    private int $sortCode;

    /**
     * @ORM\Column(type="string", length=3)
     * @Groups({"show_detail", "new"})
     */
    private string $currency;

    /**
     * @ORM\OneToMany(
     *     targetEntity=Transaction::class,
     *     mappedBy="customerAccount",
     *     orphanRemoval=true,
     *     cascade={"persist"},
     *     fetch="EAGER"
     *     )
     * @ORM\JoinColumn(
     *     name="account_no",
     *     referencedColumnName="account_no"
     * )
     * @Groups({"show_detail"})
     */
    private ?Collection $transactions;

    public function __construct(
        string $accountNo,
        string $customerName,
        int $sortCode,
        string $currency
    ) {
        $this->accountNo = $accountNo;
        $this->customerName = $customerName;
        $this->sortCode = $sortCode;
        $this->currency = $currency;
        $this->transactions = new ArrayCollection();
    }

    public function getAccountNo(): ?string
    {
        return $this->accountNo;
    }

    public function setAccountNo(string $accountNo): self
    {
        $this->accountNo = $accountNo;

        return $this;
    }

    public function getCustomerName(): ?string
    {
        return $this->customerName;
    }

    public function setCustomerName(string $customerName): self
    {
        $this->customerName = $customerName;

        return $this;
    }

    public function getSortCode(): ?int
    {
        return $this->sortCode;
    }

    public function setSortCode(int $sortCode): self
    {
        $this->sortCode = $sortCode;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setCustomerAccount($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            //@todo: is this needed?
//            if ($transaction->getCustomerAccount() === $this) {
//                $transaction->setCustomerAccount(null);
//            }
        }

        return $this;
    }

    public static function buildFromJson(Serialiser $serialiser, string $json): CustomerAccount
    {
        $object = $serialiser->serialiseEntityFromJson($json, self::class);

        return new CustomerAccount(
            $object->accountNo,
            $object->customerName,
            $object->sortCode,
            $object->currency
        );
    }
}
