<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\TransactionRepository;
use App\Services\DateHelper;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction implements Entity
{
    public const CREDIT_TYPE = 'CREDIT';
    public const DEBIT_TYPE = 'DEBIT';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show_detail", "show_list"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=CustomerAccount::class,
     *     inversedBy="transactions"
     * )
     * @ORM\JoinColumn(
     *     nullable=false,
     *     referencedColumnName="account_no",
     *     name="account_no", onDelete="CASCADE"
     * )
     * @Groups({"show_parent"})
     */
    private customerAccount $customerAccount;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"show_detail", "show_list", "new"})
     */
    private \DateTimeInterface $timestamp;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('CREDIT', 'DEBIT')")
     * @Groups({"show_detail", "show_list", "new"})
     */
    private string $type;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"show_detail", "show_list", "new"})
     */
    private float $amount = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"show_detail", "show_list", "new"})
     */
    private ?string $description;

    /**
     * @ORM\Column(type="string", length=32)
     * @Groups({"show_detail", "show_list", "new"})
     */
    private string $iban;

    public function __construct(
        ?int $id,
        customerAccount $customerAccount,
        \DateTimeInterface $timestamp,
        string $type,
        float $amount,
        ?string $description,
        string $iban
    ) {
        if ($id) {
            $this->id = $id;
        }
        $this->customerAccount = $customerAccount;
        $this->timestamp = $timestamp;
        $this->type = $type;
        $this->amount = $amount;
        $this->description = $description;
        $this->iban = $iban;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getCustomerAccount(): customerAccount
    {
        return $this->customerAccount;
    }

    public function setCustomerAccount(customerAccount $customerAccount): self
    {
        $this->customerAccount = $customerAccount;

        return $this;
    }

    public function getTimestamp(): string
    {
        return $this->timestamp->format(DateHelper::SHORT_DATE_TIME_FORMAT);
    }

    public function getTimestampObject(): \DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public static function buildFromJson(
        string $json,
        CustomerAccount $customerAccount
    ): Transaction {
        $object = json_decode($json);
        $object->customerAccount = $customerAccount;
        $object->timestamp = DateTimeImmutable::createFromFormat(DateHelper::LONG_DATE_TIME_FORMAT, $object->timestamp);

        return new Transaction(
            $object->id ?? null,
            $customerAccount,
            $object->timestamp,
            $object->type,
            $object->amount,
            $object->description,
            $object->iban,
        );
    }
}
