<?php

declare(strict_types=1);

namespace App\Controller;

use Doctrine\DBAL\Exception\ConnectionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class HealthController extends AbstractController implements TokenAuthenticatedController
{
    public function health(): JsonResponse
    {
        $response = [
            'app' => 'Service is running',
            'db' => $this->isDoctrineServiceOK() === true ?
                'Successfully connected to DB' :
                'Cannot connect to DB',
        ];
        return $this->json($response, Response::HTTP_OK);
    }

    private function isDoctrineServiceOK(): bool
    {
        try {
            $schemaManager = $this->getDoctrine()->getConnection()->getSchemaManager();
            return ($schemaManager->tablesExist(array('customer_account')) == true);
        } catch (ConnectionException $e) {
            return false;
        }
    }
}
