<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ErrorController extends AbstractController
{
    use SimpleJsonResponse;

    public function show(Request $request, $exception): JsonResponse
    {
        return $this->generateSimpleJsonResponse(
            sprintf(
                'There was an issue with resource %s. %s ',
                $request->getRequestUri(),
                $exception->getMessage(),
            ),
            Response::HTTP_NOT_FOUND
        );
    }
}
