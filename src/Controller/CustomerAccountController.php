<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\CustomerAccount;
use App\Repository\CustomerAccountRepository;
use App\Services\Serialiser;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;

final class CustomerAccountController extends AbstractController implements TokenAuthenticatedController
{
    use SimpleJsonResponse;

    private CustomerAccountRepository $customerAccountRepository;
    private Serialiser $serialiser;

    public function __construct(CustomerAccountRepository $customerAccountRepository, Serialiser $serialiser)
    {
        $this->customerAccountRepository = $customerAccountRepository;
        $this->serialiser = $serialiser;
    }

    /**
     * @SWG\Property(description="Retrieves a customer account by the account number.")
     *
     * @SWG\Response(
     *     response=200,
     *     @Model(type=CustomerAccount::class)
     * )
     */
    public function show(string $id): JsonResponse
    {
        $customerAccount = $this->customerAccountRepository->findOne($id);
        $data = $this->serialiser->serialiseArrayFromEntity($customerAccount, Serialiser::DETAIL_ENTITY_GROUP);

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function create(Request $request): JsonResponse
    {
        if (!$request->getContent()) {
            return $this->generateEmptyBodyErrorMessage();
        }

        $newCustomerAccount = CustomerAccount::buildFromJson($this->serialiser, $request->getContent());
        $returnedCustomerAccount = $this->customerAccountRepository->create($newCustomerAccount);
        $response = $this->serialiser->serialiseArrayFromEntity($returnedCustomerAccount);

        return new JsonResponse($response, Response::HTTP_OK);
    }

    public function update(Request $request, string $id): JsonResponse
    {
        if (!$request->getContent()) {
            return $this->generateEmptyBodyErrorMessage();
        }

        $customerAccount = CustomerAccount::buildFromJson($this->serialiser, $request->getContent());
        if ($customerAccount->getAccountNo() !== $id) {
            return $this->generateSimpleJsonResponse(
                sprintf(
                    'AccountNo cannot be changed from %s',
                    $id
                )
            );
        }

        $existingCustomerAccount = CustomerAccount::buildFromJson($this->serialiser, $request->getContent());
        $returnedCustomerAccount = $this->customerAccountRepository->update($existingCustomerAccount);
        $response = $this->serialiser->serialiseArrayFromEntity($returnedCustomerAccount);

        return new JsonResponse($response, Response::HTTP_OK);
    }

    public function delete(string $id): JsonResponse
    {
        $this->customerAccountRepository->delete($id);

        return $this->generateSimpleJsonResponse(
            sprintf(
                'Customer account %s has been deleted',
                $id
            )
        );
    }

    public function showAll(): JsonResponse
    {
        $customerAccounts = $this->customerAccountRepository->findAll();
        $response = array_map(function (CustomerAccount $customerAccount) {
            return $this->serialiser->serialiseArrayFromEntity($customerAccount);
        }, $customerAccounts);

        return new JsonResponse($response, Response::HTTP_OK);
    }
}
