<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait SimpleJsonResponse
{
    private function generateSimpleJsonResponse(string $message, int $status = Response::HTTP_OK): JsonResponse
    {
        return $this->json(['message' => $message], $status);
    }

    private function generateEmptyBodyErrorMessage(): JsonResponse
    {
        return $this->generateSimpleJsonResponse('The request body is empty', Response::HTTP_NOT_FOUND);
    }
}
