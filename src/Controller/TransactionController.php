<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\CustomerAccount;
use App\Entity\Transaction;
use App\Repository\CustomerAccountRepository;
use App\Repository\TransactionRepository;
use App\Services\Serialiser;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;

final class TransactionController extends AbstractController implements TokenAuthenticatedController
{
    use SimpleJsonResponse;

    private TransactionRepository $transactionRepository;
    private CustomerAccountRepository $customerAccountRepository;
    private Serialiser $serialiser;

    public function __construct(
        TransactionRepository $transactionRepository,
        CustomerAccountRepository $customerAccountRepository,
        Serialiser $serialiser
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->customerAccountRepository = $customerAccountRepository;
        $this->serialiser = $serialiser;
    }

    /**
     * @SWG\Property(description="Retrieves a transaction by its Id.")
     *
     * @SWG\Response(
     *     response=200,
     *     @Model(type=Transaction::class)
     * )
     */
    public function show(string $id): JsonResponse
    {
        $transaction = $this->transactionRepository->findOne($id);
        $data = $this->serialiser->serialiseArrayFromEntity($transaction, Serialiser::LIST_ENTITY_GROUP);

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function create(Request $request, string $accountId): JsonResponse
    {
        if (!$request->getContent()) {
            return $this->generateEmptyBodyErrorMessage();
        }

        $newTransaction = Transaction::buildFromJson(
            $request->getContent(),
            $this->getCustomerAccount($accountId)
        );
        $customerAccount = $this->customerAccountRepository->findOne($accountId);
        $customerAccount->addTransaction($newTransaction);
        $this->customerAccountRepository->update($customerAccount);
        $response = $this->serialiser->serialiseArrayFromEntity(
            $customerAccount->getTransactions()->last(),
            Serialiser::LIST_ENTITY_GROUP
        );

        return new JsonResponse($response, Response::HTTP_OK);
    }

    public function update(Request $request, string $accountId, string $id): JsonResponse
    {
        if (!$request->getContent()) {
            return $this->generateEmptyBodyErrorMessage();
        }

        $transaction = Transaction::buildFromJson($request->getContent(), $this->getCustomerAccount($accountId));
        $transaction->setId((int) $id);

        $returnedTransaction = $this->transactionRepository->update($transaction);
        $response = $this->serialiser->serialiseArrayFromEntity($returnedTransaction);

        return new JsonResponse($response, Response::HTTP_OK);
    }

    public function delete(string $accountId, string $id): JsonResponse
    {
        $this->transactionRepository->delete($id);

        return $this->generateSimpleJsonResponse(
            sprintf(
                'Transaction %s has been deleted',
                $id
            )
        );
    }

    public function showAllByCustomerAccount(string $accountId): JsonResponse
    {
        $customerAccounts = $this->transactionRepository->findAllByCustomerAccount($accountId);
        $response = array_map(function (Transaction $transaction) {
            return $this->serialiser->serialiseArrayFromEntity($transaction, Serialiser::LIST_ENTITY_GROUP);
        }, $customerAccounts);

        return new JsonResponse($response, Response::HTTP_OK);
    }

    private function getCustomerAccount(string $accountId): CustomerAccount
    {
        return  $this->customerAccountRepository->findOne($accountId);
    }
}
