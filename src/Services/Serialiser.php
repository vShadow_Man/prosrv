<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Entity;
use Symfony\Component\Serializer\SerializerInterface;

final class Serialiser
{
    public const DETAIL_ENTITY_GROUP = 'show_detail';
    public const LIST_ENTITY_GROUP = 'show_list';
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function serialiseArrayFromEntity(Entity $entity, string $group = null): array
    {
        $json = $this->serialiseJsonFromEntity($entity, $group);

        return json_decode($json, true);
    }

    public function serialiseJsonFromEntity(Entity $entity, string $group = null): string
    {
        if ($group === null) {
            $group = self::DETAIL_ENTITY_GROUP;
        }

        return $this->serializer->serialize(
            $entity,
            'json',
            ['groups' => $group]
        );
    }

    public function serialiseEntityFromJson(string $data, string $entityClass): object
    {
        return $this->serializer->deserialize($data, $entityClass, 'json');
    }
}
