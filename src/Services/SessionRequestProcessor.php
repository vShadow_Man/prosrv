<?php

declare(strict_types=1);

namespace App\Services;

use Symfony\Component\HttpFoundation\Session\Session;

final class SessionRequestProcessor
{
    private Session $session;
    private ?string $token = null;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function processRecord(array $record): array
    {
        if (null === $this->token) {
            try {
                $this->token = substr($this->session->getId(), 0, 8);
            } catch (\RuntimeException $e) {
                $this->token = '????????';
            }
            $this->token .= '-' . substr(uniqid(), -8);
        }
        $record['extra']['token'] = $this->token;

        return $record;
    }
}
