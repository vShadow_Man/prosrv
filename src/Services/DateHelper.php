<?php

declare(strict_types=1);

namespace App\Services;

final class DateHelper
{
    public const LONG_DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    public const SHORT_DATE_TIME_FORMAT = 'd/m/Y H:i';
}
