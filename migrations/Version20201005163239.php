<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201005163239 extends AbstractMigration
{
    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        parent::__construct($connection, $logger);
        $this->connection->getSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE customer_account (account_no VARCHAR(8) NOT NULL, customer_name VARCHAR(255) NOT NULL, sort_code VARCHAR(6) NOT NULL, currency VARCHAR(3) NOT NULL, PRIMARY KEY(account_no)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, account_no VARCHAR(8) NOT NULL, timestamp DATETIME NOT NULL, type ENUM(\'CREDIT\', \'DEBIT\'), amount NUMERIC(10, 2) NOT NULL, description VARCHAR(255) DEFAULT NULL, iban VARCHAR(32) NOT NULL, INDEX IDX_723705D143F810F5 (account_no), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D143F810F5 FOREIGN KEY (account_no) REFERENCES customer_account (account_no) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D143F810F5');
        $this->addSql('DROP TABLE customer_account');
        $this->addSql('DROP TABLE transaction');
    }
}